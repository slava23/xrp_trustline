import { FireblocksSDK } from "fireblocks-sdk";
import * as ripple from 'ripple-lib';
import * as xrp_signer from './src/xrp-signer'
import * as fs from 'fs';
import * as path from 'path'



const API_KEY = '<api_key>';
const PATH_TO_SECRET_KEY = '<path_to_secret_key>'
const VAULT_ACCOUNT_ID = '';

// Initialize Fireblocks API Client
const apiKey = API_KEY
const apiSecret = fs.readFileSync(path.resolve(__dirname, PATH_TO_SECRET_KEY), "utf8"); 
const fireblocksApiClient = new FireblocksSDK(apiSecret, apiKey);


// vault account of the source XRP address



const api = new ripple.RippleAPI({
//'wss://s1.ripple.com' // Public ripple server
  server: 'wss://xrplcluster.com'
});

api.connect().then(async () => {

  const account = await xrp_signer.getAddress(fireblocksApiClient, VAULT_ACCOUNT_ID);
  console.log("Account address", account);
  
  let note = `Setting TrustLine for Solo AirDrop, for XRP address: ${account}`;
  
  await xrp_signer.signAndSubmitTransaction(api, fireblocksApiClient, VAULT_ACCOUNT_ID, {
    "TransactionType": "TrustSet",
    "Account": account,
    "Fee": "15000",
    "Flags": 262144,
    "LimitAmount": {
      "currency": '534F4C4F00000000000000000000000000000000',
      "issuer": "rsoLo2S1kiGeCcn6hCUXVrCpGMWLrRrLZz",
      "value": "200000000"
    },
  },
  note)  
});
