import { FireblocksSDK } from "fireblocks-sdk";
import * as ripple from 'ripple-lib';
import * as xrp_signer from './src/xrp-signer'
import * as fs from 'fs';
import * as path from 'path'


const API_KEY: string = 'your_api_key';
const PATH_TO_SECRET_KEY: string = 'path_to_secret_key'

const VAULT_ACCOUNT_ID: number = 0;

//Enter the domain name as a string, for example: test.com
const DOMAIN: string = null 



// Initialize Fireblocks API Client
const apiKey = API_KEY
const apiSecret = fs.readFileSync(path.resolve(__dirname, PATH_TO_SECRET_KEY), "utf8"); 
const fireblocksApiClient = new FireblocksSDK(apiSecret, apiKey);

const api = new ripple.RippleAPI({
//'wss://xrplcluster.com', 'wss://s1.ripple.com'// Public ripple servers
  server: 'wss://xrplcluster.com'
});

async function createTx(account, domain?){

  if(domain){
    console.log("Text domain:", Buffer.from(domain, 'hex').toString());
    console.log("Hex Domain:",domain)
    await xrp_signer.signAndSubmitTransaction(api, fireblocksApiClient, VAULT_ACCOUNT_ID, {
      "TransactionType": "AccountSet",
      "Account": account,
      "Fee": "1000",
      "Domain": domain,
      "SetFlag": 1 // --> Enable dest tag required 
      //"ClearFlag": 1 // --> Disable test tag required
    },
    `Set destination tag required flag and domain ${Buffer.from(domain, 'hex').toString()} for vault account ${VAULT_ACCOUNT_ID}`
    )
  }else{
    await xrp_signer.signAndSubmitTransaction(api, fireblocksApiClient, VAULT_ACCOUNT_ID, {
      "TransactionType": "AccountSet",
      "Account": account,
      "Fee": "1000",
      "Domain": "",
      "SetFlag": 1 // --> Enable dest tag required 
      //"ClearFlag": 1 // --> Disable test tag required
    },
    `Set destination tag required flag for vault account ${VAULT_ACCOUNT_ID}`
    )
  }
}


api.connect().then(async () => {

  const account = await xrp_signer.getAddress(fireblocksApiClient, VAULT_ACCOUNT_ID);
  
  if(DOMAIN){
    const domain = Buffer.from(DOMAIN.toLowerCase()).toString('hex').toUpperCase();
    await createTx(account, domain)
  }else{
    await createTx(account)
  }
});
