# XRP Tools 

RAW signing scripts for the following scenarions:

**1. Set 'Require tag' and 'Domain' configurations on your XRP wallet**

**2. Set TrustLine in XRP (SOLO Air Drop)**

**3.Move IOU tokens out of your Fireblocks vault**




## Set 'Require Tag' configuration:

Clone the repo:
``` 
git clone https://gitlab.com/slava23/xrp_trustline.git 

cd xrp_trustline
```

Install:
```
npm install
```

Edit the setDestTag.ts file:
```
API_KEY - the API Key you got from Fireblocks
PATH_TO_SECRET_KEY - the path to your RSA private key (for example: '~/secrets/fireblocks_secret.key')
VAULT_ACCOUNT_ID - the vault account ID of the XRP wallet to set the require tag configuration
```

Run:
```
ts-node setDestTag.ts
```

Sign the RAW signing operation in your Fireblocks Mobile app.
Make sure that you have the following result printed out to the console:
```
resultCode: 'tesSUCCESS',
resultMessage: 'The transaction was applied. Only final in a validated ledger.'
.
.
.
hash: '<some_hash>'
```
Check the transaction's hash in the XRP block explorer and make sure that the transaction was executed successfully.


## Set TrustLine:

Clone the repo:
``` 
git clone https://gitlab.com/slava23/xrp_trustline.git 

cd xrp_trustline
```

Install:
```
npm install
```

Edit the index.ts file:
```
API_KEY - the API Key you got from Fireblocks
PATH_TO_SECRET_KEY - the path to your RSA private key (for example: '~/secrets/fireblocks_secret.key')
VAULT_ACCOUNT_ID - the vault account ID of the XRP wallet to set the TrustLine in (for example: 1)
```

Run:
```
ts-node index.ts
```

Sign the RAW signing operation in your Fireblocks Mobile app.
Make sure that you have the following result printed out to the console:
```
resultCode: 'tesSUCCESS',
resultMessage: 'The transaction was applied. Only final in a validated ledger.'
.
.
.
hash: '<some_hash>'
```
Check the transaction's hash in the XRP block explorer and make sure that the transaction was executed successfully.

## Transfer tokens:

Edit the transferTokens.ts file:

```
DESTINATION - the destination address (Make sure that the TrustLine is set on the destination Address)
API_KEY - the API Key you got from Fireblocks
PATH_TO_SECRET_KEY - the path to your RSA private key (for example: '~/secrets/fireblocks_secret.key')
VAULT_ACCOUNT_ID - the vault account ID of the XRP wallet to set the TrustLine in (for example: 1)
amount - the amount you wish to move (string) OR null for the entire balance
```
**Note: When moving the entire balance of the token, there might be a chance of an insignificant token leftover in the wallet**

Run:
```
ts-node index.ts
```

Sign the RAW signing operation in your Fireblocks Mobile app.
Make sure that you have the following result printed out to the console:
```
resultCode: 'tesSUCCESS',
resultMessage: 'The transaction was applied. Only final in a validated ledger.'
.
.
.
hash: '<some_hash>'
```
Check the transaction's hash in the XRP block explorer and make sure that the transaction was executed successfully.

## Please NOTE:
Since XRP transactions require lastLedgerSequence as a part of the transaction's body, when using RAW signing it is limited for a quite short period of time (up to 2 mins), which means that the transcation should be signed immediately after the submission. Waiting for longer signing time will result in unsuccessful transaction's broadcasting.
