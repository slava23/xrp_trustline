import { FireblocksSDK } from "fireblocks-sdk";
import * as ripple from 'ripple-lib';
import * as xrp_signer from './src/xrp-signer'
import * as fs from 'fs';
import * as path from 'path'

//SOLO Token ID - DO NOT CHANGE
const TOKEN_ID: string = '534F4C4F00000000000000000000000000000000' 

//Destination address that has the token's TrustLine in place
const DESTINATION: string = "<destination_address>"
//API Key
const API_KEY: string = '<your_api_key>';
//Secret key path
const PATH_TO_SECRET_KEY: string = '<path_to_your_api_secret_key>'
//Vault account ID
const VAULT_ACCOUNT_ID: number = 0;
/*
Amount to transfer or null for the entire balance
Please NOTE that there might be a small leftover of the balance after the transaction
*/
const amount = null;


// Initialize Fireblocks API Client
const apiKey = API_KEY
const apiSecret = fs.readFileSync(path.resolve(__dirname, PATH_TO_SECRET_KEY), "utf8"); 
const fireblocksApiClient = new FireblocksSDK(apiSecret, apiKey);

//Public ripple servers - different web sockets options
const api = new ripple.RippleAPI({
//'wss://xrplcluster.com', 'wss://s1.ripple.com'
  server: 'wss://xrplcluster.com'
});


async function createTx(account, amountToTransfer, balance){
  
  await xrp_signer.signAndSubmitTransaction(api, fireblocksApiClient, VAULT_ACCOUNT_ID, {
    "TransactionType": "Payment",
      "Account": account,
      "Destination": DESTINATION,
      "Fee": "1000",
      "SendMax":{
        "currency":TOKEN_ID,
        "issuer":"rsoLo2S1kiGeCcn6hCUXVrCpGMWLrRrLZz",
        "value": balance
      },
      "Amount": {
        "currency": '534F4C4F00000000000000000000000000000000',
        "issuer": "rsoLo2S1kiGeCcn6hCUXVrCpGMWLrRrLZz",
        "value": amountToTransfer
      },
  },
  `Transferring ${amountToTransfer} SOLO tokens from address ${account} to address ${DESTINATION}`
  );
}


api.connect().then(async () => {

  const account = await xrp_signer.getAddress(fireblocksApiClient, VAULT_ACCOUNT_ID);
  console.log("Account address", account);

  try{
      const res = await api.request("account_lines",{
        "account": account,
      })
      let balance;
      let soloLine;
      
      for (let i=0; i<res.lines.length; i++){
        if(res.lines[i].currency == TOKEN_ID){
          balance = res.lines[i].balance;
          soloLine = res.lines[i];
          break;
        }
      }

      if(soloLine){
        console.log("SOLO balance:", balance);
        
        if(!amount){
          let amountToTransfer = String((parseFloat(balance)- 0.001).toFixed(15))
          console.log(`Transferring an amount of ${amountToTransfer} SOLO tokens`);
          await createTx(account, amountToTransfer, balance)
          api.disconnect();
        }else{
          if(parseFloat(balance) < parseFloat(amount)){
            throw new Error (`Insufficient balance to transfer ${amount}, available balance: ${balance}`)
          }else{
            if(parseFloat(amount) == parseFloat(balance)){
              let amountToTransfer = String((parseFloat(amount)- 0.0001).toFixed(15))
              console.log(`Transferring an amount of ${amountToTransfer} SOLO tokens`);
              await createTx(account, amountToTransfer, balance)
              api.disconnect();
            }else{
              await createTx(account, amount, balance)
              console.log(`Transferring an amount of ${amount} SOLO tokens`);
              api.disconnect();
            }
          }
        }
    }else{
      throw new Error ("There is no SOLO trustline set on the provided account")
    }
  }catch(e){
    console.log(e);
  }
});
